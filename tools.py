from math import sqrt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from typing import Tuple, Dict

from matplotlib.patches import Rectangle

from sklearn.pipeline import Pipeline
from sklearn.model_selection import learning_curve, GridSearchCV, RepeatedKFold
from sklearn.metrics import (
    mean_absolute_error,
    mean_squared_error,
    median_absolute_error,
    r2_score,
)

from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.linear_model import LassoCV, ElasticNetCV, LinearRegression


def big_fit(X_train, y_train, X_test, y_test):
    estimators = [
        ("lr", LinearRegression()),
        ("rfr", RandomForestRegressor(random_state=42)),
        ("gbr", GradientBoostingRegressor(random_state=42)),
    ]

    CVestimators = [
        ("lasso", LassoCV(random_state=42, max_iter=5000)),
        ("elastic", ElasticNetCV(random_state=42, max_iter=5000)),
    ]

    default_scores = dict()

    print("Training with defaults...")

    for estimator in estimators:
        name = estimator[0]
        model = estimator[1]

        model.fit(X_train, y_train)

        y_pred = model.predict(X_test)

        scores = [
            int(mean_absolute_error(y_test, y_pred)),
            int(sqrt(mean_squared_error(y_test, y_pred))),
            int(median_absolute_error(y_test, y_pred)),
            round(r2_score(y_test, y_pred) * 100, 2),
        ]
        default_scores[name] = scores

    param_grids = [
        {"fit_intercept ": [True, False]},
        {
            "n_estimators": np.arange(100, 600, 100),
            "max_features": ["sqrt", "log2", 0.2, 0.3, 0.4],
        },
        {
            "n_estimators": np.arange(100, 600, 100),
            "max_features": ["sqrt", "log2", 0.2, 0.3, 0.4],
        },
    ]

    best_estimators = dict()
    best_params = dict()
    best_scores = dict()
    predict_scores = dict()

    print("Training with grid...")

    for estimator, param_grid in zip(estimators, param_grids):
        grid = GridSearchCV(
            estimator[1],
            param_grid,
            cv=5,
            scoring="neg_mean_squared_error",
            return_train_score=True,
            verbose=3,
            n_jobs=8,
            error_score="raise",
        )
        name = estimator[0]

        print(f"Training with {name}")

        grid.fit(X_train, y_train)
        best_estimators[name] = grid.best_estimator_
        best_params[name] = grid.best_params_
        best_scores[name] = round(grid.best_score_, 2)

        y_pred = grid.predict(X_test)
        scores = [
            int(mean_absolute_error(y_test, y_pred)),
            int(sqrt(mean_squared_error(y_test, y_pred))),
            int(median_absolute_error(y_test, y_pred)),
            round(r2_score(y_test, y_pred) * 100, 2),
        ]
        predict_scores[name] = scores

    return default_scores, best_scores, predict_scores, best_params, best_estimators


def plot_learning_curve(estimator, X, y):
    plt.figure(figsize=(6, 6))
    train_sizes, train_scores, val_scores = learning_curve(
        estimator,
        X,
        y,
        cv=10,
        scoring="r2",
        n_jobs=8,
        train_sizes=np.linspace(0.1, 1.0, 25),
    )
    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)

    val_mean = np.mean(val_scores, axis=1)
    val_std = np.std(val_scores, axis=1)
    plt.subplots(1, figsize=(10, 10))
    plt.plot(train_sizes, train_mean, color="red", label="Training score")
    plt.plot(train_sizes, val_mean, color="blue", label="Cross-validation score")

    plt.fill_between(
        train_sizes,
        train_mean - train_std,
        train_mean + train_std,
        color="red",
        alpha=0.2,
    )
    plt.fill_between(
        train_sizes, val_mean - val_std, val_mean + val_std, color="blue", alpha=0.2
    )

    plt.title("Learning Curve")
    plt.xlabel("Training Set Size")
    plt.ylabel("R² Score")
    plt.legend(loc="best")
    plt.tight_layout()
    plt.show()


def plot_error_distribution(y_test, y_pred):
    err_hist = np.abs(y_test - y_pred)
    median, p25, p75, p95 = (
        np.percentile(err_hist, 50),
        np.percentile(err_hist, 25),
        np.percentile(err_hist, 75),
        np.percentile(err_hist, 95),
    )
    print(p25, median, p75)
    # Colours for different percentiles
    p25_colour = "gold"
    p50_colour = "mediumaquamarine"
    p75_colour = "deepskyblue"
    p95_colour = "peachpuff"
    fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(10, 10))
    axes[0].boxplot(err_hist, 0, "gD", vert=False)
    axes[0].axvline(median, color=p50_colour, alpha=0.6, linewidth=0.5)
    axes[0].axis("off")

    counts, bins, patches = axes[1].hist(
        err_hist, bins=50, facecolor=p50_colour, edgecolor="black"
    )
    # Change the colors of bars at the edges
    twentyfifth, seventyfifth, ninetyfifth = np.percentile(err_hist, [25, 75, 95])
    for patch, leftside, rightside in zip(patches, bins[:-1], bins[1:]):
        if rightside < twentyfifth:
            patch.set_facecolor(p25_colour)
        elif leftside > ninetyfifth:
            patch.set_facecolor(p95_colour)
        elif leftside > seventyfifth:
            patch.set_facecolor(p75_colour)

    y_annot = axes[1].get_yticks()[-2]

    axes[1].axvline(median, color=p50_colour)
    axes[1].annotate("50%", xy=(median, y_annot - 5))
    axes[1].axvline(p25, color=p25_colour)
    axes[1].annotate("25%", xy=(p25, y_annot))
    axes[1].axvline(p75, color=p75_colour)
    axes[1].annotate("75%", xy=(p75, y_annot))
    axes[1].axvline(p95, color=p95_colour)
    axes[1].annotate("95%", xy=(p95, y_annot))

    # Set the ticks to be at the edges of the bins.
    axes[0].set_xticks(np.arange(0, 250000, 20000))
    plt.xticks(rotation=70)

    # create legend
    handles = [
        Rectangle((0, 0), 1, 1, color=c, ec="k")
        for c in [p50_colour, p75_colour, p95_colour]
    ]
    labels = ["25-50 Percentile", "50-75 Percentile", ">95 Percentile"]
    plt.legend(handles, labels, bbox_to_anchor=(0.5, 0.0, 0.80, 0.99))

    plt.subplots_adjust(hspace=0)
    plt.show()


def plot_feature_importance(
    data: pd.DataFrame,
    fitted_model,
    feature_names=None,
    max_features=20,
    only_cols=None,
):
    feature_importance = np.array(fitted_model.feature_importances_)
    if feature_names is None:
        feature_names = np.array(data.columns)

    fi_df = pd.DataFrame(
        {
            "feature_names": feature_names,
            "feature_importance": feature_importance,
        }
    )

    if only_cols is not None:
        fi_df = fi_df[fi_df["feature_names"].isin(only_cols)]

    fi_df.sort_values(by=["feature_importance"], ascending=False, inplace=True)

    fi_df = fi_df.head(max_features)

    plt.figure(figsize=(19, 15))

    sns.barplot(x=fi_df["feature_importance"], y=fi_df["feature_names"])

    plt.title(f"{fitted_model.__class__.__name__} feature importance")
    plt.xlabel("feature importance")
    plt.ylabel("feature names")


def plot_coef_importances(
    X_enc, fitted_model, feature_names=None, max_features=20, only_cols=None
):
    if feature_names is None:
        feature_names = fitted_model.feature_names_in_

    X_preprocessed = pd.DataFrame(X_enc.todense(), columns=feature_names)
    coefs_importance = pd.DataFrame(
        {
            "feature_names": feature_names,
            "coeff_importance": fitted_model.coef_ * X_preprocessed.std(axis=0),
        }
    )

    if only_cols is not None:
        coefs_importance = coefs_importance[
            coefs_importance["feature_names"].isin(only_cols)
        ]

    coefs_importance.sort_values(
        by="coeff_importance", ascending=False, key=lambda x: abs(x), inplace=True
    )

    coefs_importance = coefs_importance.head(max_features)

    plt.figure(figsize=(19, 15))

    sns.barplot(
        x=coefs_importance["coeff_importance"], y=coefs_importance["feature_names"]
    )

    plt.title(
        f"{fitted_model.__class__.__name__} top {max_features} feature importance"
    )
    plt.xlabel("feature importance")
    plt.ylabel("feature names")
